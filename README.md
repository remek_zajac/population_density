## Dependencies
Install [nvm](https://github.com/nvm-sh/nvm) and set the version to v8.17.0

## Local Tile Server
In the root of the tiles directory structure:
```
npm install http-server -g
http-server -p 8088 --cors
```

## Web Viewer
```
npm install
npm run gulp
```
